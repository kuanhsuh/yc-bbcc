<div class="author-info">
  <div class="author-image">
    <a href=" ">
    <?php echo get_avatar(get_the_author_meta('ID'), 120, '', '', array('class' => 'avatar-photo')); ?>
    </a>
  </div>
  <div class="author-bio">
    <h4>關於作者: <?php echo get_the_author_meta("display_name");  ?></h4>
    <p><?php echo get_the_author_meta("description");  ?></p>
  </div>
</div>