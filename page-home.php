<?php
/* Template Name: Home Page */
get_header();?>
<!--Begin Hero Section-->
<section id="home" class="pd-tp-60">
      <div class="container-page  hero" style="background-image: url(<?php bloginfo('template_directory');?>/assets/images/custom/home.jpg)">
        <div class="col-md-12 col-xs-12 col-sm-12">
          <div class="hero-text">
            <h2>你在尋找專業的網頁設計？</h2>
            <div id="typed-strings">
              <b>公司官網</b>
              <b>電子商務</b>
              <b>SEO 優化</b>
              <b>網路行銷</b>
            </div>
            <span id="typed"></span>
            <p>別讓你的網頁成為你的豬的隊友</p>
            <a href="#about" class="btn btn-red">關於我們</a>
          </div>
        </div>
      </div>
    </section>
    <section id="services" class="pd-tp-80 pd-bt-40">
      <div class="container-page">
        <div class="row">
          <div class="row-centered">
            <div class="col-centered col-lg-7">
              <h2 class="title-h2">我們為您提供的服務</h2>
              <p class="font-p mg-tp-30 mg-bt-40">
              以最專業的角度，從各方面分析網路用戶的習性及愛好，為您提供高品質數位行銷，增加網上知名度，使貴公司擁有最優質的競爭力！
              </p>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="service-block">
              <div class="icon-service">
                <svg>
                  <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-magic-wand"></use>
                </svg>
              </div>
              <h3>網頁＆網頁系統 設計 </h3>
              <p>
              從公司Logo到購物車，
              由小細節到整體頁面；
              我們為您提供一條龍的服務，幫您全方位提升門面形象。
              </p>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="service-block">
              <div class="icon-service">
                <svg>
                  <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-brain"></use>
                </svg>
              </div>
              <h3>SEO 優化</h3>
              <p>
              讓客戶第一眼就找到您！幫助提高網頁搜索的排名，讓您的公司價值更進一步。
              </p>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="service-block">
              <div class="icon-service">
              <svg>
                <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-megaphone"></use>
              </svg>
              </div>
              <h3>數位行銷</h3>
              <p>
              使用FB, Google 廣告等數位媒體，將您的服務、產品或廣告清晰、簡潔且高頻率的呈現給各個網路用戶。
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--About-->
    <section id="about">
      <!--container-->
      <div class="container-page bg-color padd-80">
        <div class="container">
          <div class="row ">
            <div class="col-lg-5 col-md-5">
              <h2 class="title-h2">為什麼選我們？</h2>
              <p class="font-p mg-tp-30 mg-bt-30">
              我們擁有網頁工程師的背景，對於程式品質具有極高的自我要求。在電子商務及數位行銷上更有著豐富的經驗。
              </p>
              <p class="font-p mg-tp-30 mg-bt-30">
              我們有能力為您提供從網頁設計到數位行銷的高品質一條龍服務，讓貴公司輕鬆擁有最佳的網上競爭力並能大幅度提升公司商業價值！
              </p>
              <!-- <a href="" class="btn btn-red">Get started</a> -->
            </div>
            <div class="col-lg-7 col-md-7">
              <div class="row">
                <div class="col-md-6 col-lg-6">
                  <div class="icon-block ">
                    <svg>
                      <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-browser"></use>
                    </svg>
                    <h3>響應式網頁</h3>
                    <p>幫您架設能用於手機、平板或電腦等數位商品的網頁。讓客戶隨時隨地都能掌握貴公司的最新動態。</p>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="icon-block ">
                    <svg>
                      <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-startup"></use>
                    </svg>
                    <h3>網站優化</h3>
                    <p>優化您的網站程式碼，使您的網頁在Google或其他搜索引擎的排名更靠前，大眾搜尋率更高。
                    </p>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="icon-block ">
                    <svg>
                      <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-bag"></use>
                    </svg>
                    <h3>商業價值</h3>
                    <p>一流的產品更需要一流的行銷。我們用我們的專業，把你們的好，堆廣給全世界，讓世界一起看到您，一起發現你們的好。</p>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="icon-block ">
                    <svg>
                      <use xlink:href="<?php bloginfo('template_directory');?>/assets/images/sprite.svg#icon-chat"></use>
                    </svg>
                    <h3>客戶溝通</h3>
                    <p>我們用心傾聽每位客戶的想法及意見，並以最專業的態度，認真、耐心的幫您完成您交予我們的託付。</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--container-->
    </section>
    <section id="numbers">
      <div class="container-page  bg-color-1 padd-40">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-8">
              <div class="row row-mag">
                <div class="number-block  col-lg-3 col-md-3">
                  <span>3</span>
                  <small>網頁 </small>
                </div>
                <div class="number-block   col-lg-3 col-md-3">
                  <span>3</span>
                  <small>開心的客戶</small>
                </div>
                <div class="number-block  col-lg-3 col-md-3">
                  <span>1000+</span>
                  <small>增加的新訪客</small>
                </div>
                <div class="number-block  col-lg-3 col-md-3">
                  <span>970</span>
                  <small>行程式編碼 </small>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="number-text">
                <p>我們為您創造的，是夢想實現的世界！</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="portfolio">
      <div class="container-page padd-80">
        <div class="row row-mag">
          <div class="row-centered">
            <div class="col-centered col-lg-7 col-md-7">
              <h2 class="title-h2">最新作品</h2>
              <p class="font-p mg-tp-30 mg-bt-30">
                獨一無二的作品，獻給擁有偉大夢想的您
              </p>
            </div>
            <div class="project-holder">
              <div class="projetc-inner">
                <div class="row">
                  <?php
                    $args = array(
                    'post_type' => array('optimization', 'project'),
                    'posts_per_page'=>3,
                    );
                    $project_query = new WP_Query($args);
                    while ($project_query->have_posts()) : $project_query->the_post();
                  ?>
                  <div class="col-lg-4  col-md-4">
                    <div class="blog-inner">
                      <div class="blog-image">
                      <?php if (has_post_thumbnail()) {
                      the_post_thumbnail('medium', array( 'class'  => 'img-responsive' ));
                  } else {
                      ?>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/blog1.jpg" alt="">
                <?php
                  } ?>
                      </div>
                      <div class="blog-inner-content">
                        <h3>
                          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h3>
                        <div class="comment-blog">
                          <ul>
                            <li>
                              <a href="">
                                <i class="icon_ribbon_alt"></i><?php echo get_post_type_object(get_post_type())->label; ?></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endwhile;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="blog">
      <div class="container-page bg-color padd-80">
        <div class="row">
          <div class="row-centered">
            <div class="col-centered col-lg-7">
              <h2 class="title-h2">最新文章</h2>
              <p class="font-p mg-tp-30 mg-bt-60">
              這裡有許多訊息想分享給您，希望您能在其中找到與我們相同的樂趣。
              </p>
            </div>
          </div>
          <?php
          $args = array(
            'cat' => '1',
            'posts_per_page'=>3,
            );

          $query = new WP_Query($args);

          while ($query->have_posts()) : $query->the_post(); ?>

          <div class="col-lg-4  col-md-4">
            <div class="blog-inner">
              <div class="blog-image">
              <?php if (has_post_thumbnail()) {
              the_post_thumbnail('medium', array( 'class'  => 'img-responsive' ));
          } else {
              ?>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/blog1.jpg" alt="">
                <?php
          } ?>
                <div class="blog-date">
                  <strong><?php echo get_the_date('j'); ?></strong>
                  <span><?php echo get_the_date('F'); ?></span>
                </div>
              </div>
              <div class="blog-inner-content">
                <h3>
                  <a href="<?php the_permalink()?>"><?php the_title(); ?></a>
                </h3>
                <div class="comment-blog">
                  <ul>
                    <li>
                      <a href="">
                        <i class="icon_ribbon_alt"></i><?php incomplete_cat_list(', '); ?></a>
                    </li>
                  </ul>
                </div>
                <p><?php echo excerpt(30) ?>
                </p>
              </div>
            </div>
          </div>
          <?php endwhile;
          wp_reset_query();?>
        </div>
      </div>
    </section>
    <section id="testimonials" class="padd-80 bg-color-1 clearfix">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="quote">
              <i class="icon_quotations"></i>
            </div>
            <h2 class="title-h2 color-white">我們的客戶的評價</h2>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="testimonial-caroussel carousel">
              <div class="item">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="avatar-item">
                      <img src="<?php bloginfo('template_directory');?>/assets/images/testimonial/testimonial-pembridge.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-9">
                    <div class="item-inner">
                      <p>
                      大力推薦!
                      網頁設計真的很漂亮，服務品質也很優，客服態度很親切，很棒!
                      </p>
                      <div class="rating">
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                      </div>
                      <div class="avatar-info">
                        <h4>C-Ning Yang</h4>
                        <span>磐橋進口傢俱，經理</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="avatar-item">
                      <img src="<?php bloginfo('template_directory');?>/assets/images/testimonial/testimonial-kuannyi.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-9">
                    <div class="item-inner">
                      <p>
                      專業！ 我會推薦給朋友， 謝謝。成功的幫我們提升搜尋排名。
                      </p>
                      <div class="rating">
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                      </div>
                      <div class="avatar-info">
                        <h4>David Huang</h4>
                        <span>冠億建設，經理</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="avatar-item">
                      <img src="<?php bloginfo('template_directory');?>/assets/images/testimonial/testimonial-wlgroup.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-9">
                    <div class="item-inner">
                      <p>
                        網頁品質佳，相當值得推薦的好廠商！
                      </p>
                      <div class="rating">
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                        <span class="icon_star"></span>
                      </div>
                      <div class="avatar-info">
                        <h4>Richard Wang</h4>
                        <span>立揚集團</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--sponsors clients-->
    <section id="sponsors">
      <div class="container-page pd-tp-80 pd-bt-60">
        <!--container-->
        <div class="container ">
          <div class="row ">
            <?php get_template_part('template-parts/content', 'images'); ?>
          </div>
        </div>
      </div>
    </section>
    <!--End sponsors clients-->
<?php
get_footer();
