<div class="col-lg-4 col-md-4">
    <div class="sidebar-side">
        <!-- Search sidebar-->
        <div class="sidebar-search">
            <?php get_search_form(); ?>
        </div>
        <!-- Search sidebar-->
        <div class="sidebar-categroies">
            <!-- Sidebar categories-->
            <h3>🏷 文章類別</h3>
            <ul class="catgeorie-list">
            <?php
                $categories = get_categories('exclude=1');
                foreach ($categories as $category) {
                    echo '<li>
                            <a href="' . get_category_link($category->term_id) . '" class="clearfix">
                                <span>' . $category->name . '</span>
                            </a>
                        </li>';
                }
            ?>
            </ul>
            <!-- Sidebar categories-->
        </div>
        <div class="sidebar-tags">
                <!-- Tags sidebar-->
            <h3>🏷 標籤</h3>
            <div class="tags-inner">
            <?php
                $tags = get_tags(array(
                    'hide_empty' => false
                ));
                foreach ($tags as $tag) {
                    $tag_link = get_tag_link($tag->term_id);
                    echo "<a href='{$tag_link}' title='{$tag->name} Tag'>" . $tag->name . "</a>";
                }
                ?>
            </div>
            <!-- Tags sidebar-->
    </div>
</div>