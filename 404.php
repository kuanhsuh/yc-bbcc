<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package studiod
 */

get_header();
?>

<section class="pd-tp-80">
  <div class="container-page  portflio-section-title bg-portflio-section padd-title">
    <div class="row-centered pd-tp-50">
      <div class="col-centered col-lg-7">
        <h2 class="title-h2">404 <br />糟糕😅！我們無法找到你要找的頁面</h2>
      </div>
    </div>
  </se>
</section>

<?php
get_footer();
