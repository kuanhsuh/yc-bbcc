<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package studiod
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
  <link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/assets/images/favicon.ico" type="image/x-icon">

	<?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target=".navbar-default" data-offset="100" <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'studiod'); ?></a>
	<div class="warpper clearfix">
    <!--sidebar menu-->
    <!-- Header -->
    <header class="navbar-header">
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
          <a class="navbar-brand black" href="<?php echo home_url(); ?>">
            <img src="<?php bloginfo('template_directory');?>/assets/images/logo.svg" alt="" class="logo-img">
            鎰誠數位
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon icon_menu"></span>
          </button>
          <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a data-scroll="" class="nav-link section-scroll" href="<?php echo home_url(); ?>">首頁</a>
              </li>
              <li class="nav-item">
                <a data-scroll="" class="nav-link section-scroll" href="<?php echo get_page_link(2); ?>">作品</a>
              </li>
              <li class="nav-item">
                <a data-scroll="" class="nav-link section-scroll" href="<?php echo get_page_link(47); ?>">部落格</a>
              </li>
              <li>
                <a data-scroll="" href="<?php echo get_page_link(44); ?>" class="nav-link section-scroll">聯絡</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <!--Header-->
