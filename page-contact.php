<?php
  /* Template Name: Contact Page */
  get_header();?>
<!-- section title  -->
<div class="section-title pd-tp-80">
  <div class="container-page bg-portflio-section portflio-section-title padd-title" style="background-image: url(<?php bloginfo('template_directory');?>/assets/images/custom/contact.jpg)">
    <div class="row-centered pd-tp-50">
      <div class="col-centered col-lg-7">
        <h2 class="title-h2 white">Contact Us</h2>
        <p class="font-p white">
          如果有網頁，數位行銷，和SEO相關的問題，請跟我聯絡！
        </p>
      </div>
    </div>
  </div>
</div>
<!-- section title  -->
<section id="contact">
  <div class="container-page  bg-color">
    <div class="contact-inner padd-80">
      <div class="row-centered">
        <div class="col-centered col-lg-6">
          <div class="content-map">
          <h3 class="mb-4">聯絡我們</h3>
          <p>
            <i class=" icon_pin_alt"></i> 高雄市三民區義華路384號12F</p>
          <p>
            <i class=" icon_phone"></i> 0981-606-580</p>
          <!-- <p>
            <i class=" icon_tag_alt"></i> 07 333-4444</p> -->
          <p>
            <i class=" icon_mail_alt"></i> kuanhsuh@gmail.com</p>
          </div>
        </div>
      </div>
    </div>
    <div class="map-inner">
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3682.2714636744627!2d120.33127995020017!3d22.643665135992737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e04d05d9cf079%3A0x8b384df8bddb3463!2zODA36auY6ZuE5biC5LiJ5rCR5Y2A576p6I-v6LevMzg06Jmf!5e0!3m2!1szh-TW!2stw!4v1534949251016"
          width="600" height="450" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
