<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package studiod
 */

get_header();
?>
<?php
    while (have_posts()): the_post();?>
<!-- section title  -->
<div class="section-title mt-64">
      <div class="container-page  portflio-section-title bg-portflio-section padd-title">
        <?php the_post_thumbnail('large'); ?>
        </div>
      </div>
    </div>
    <!-- section title  -->
	<!-- Blog content -->
    <div class="blog-single-post single-padd">
      <!-- container page -->
      <div class="container-page ">
        <div class="row ">
          <div class="col-lg-8 col-md-8">
            <h1 class="blog-title"><?php the_title(); ?></h1>
            <div class="blog-meta">
              <ul>
                <li><?php the_time('F j, Y'); ?></li>
                <li><?php the_category(", ", ""); ?></li>
                <li>No comments </li>
                <li><?php the_tags(null, ", "); ?></li>
              </ul>
            </div>
            <p><?php the_content(); ?></p>
            <?php get_template_part('template-parts/content', 'share'); ?>
            <?php endwhile;?>

            <!-- <div class="commnent-blog">
              <h3>3 Comments</h3>
              <div class="comment-blog-inner">
                <div class="avatar-blog">
                  <img src="<?//php bloginfo('template_directory');?>/assets/images/team/team3.jpg" alt="">
                </div>
                <div class="comment-blog-text">
                  <span>John Marconi</span>
                  <small>August 7, 2018</small>
                  <p>
                    Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis
                    doloribus asperiores repellat.
                  </p>
                </div>
              </div>
              <div class="comment-blog-inner">
                <div class="avatar-blog">
                  <img src="<?//php bloginfo('template_directory');?>/assets/images/team/team4.jpg" alt="">
                </div>
                <div class="comment-blog-text">
                  <span>John Marconi</span>
                  <small>August 7, 2018</small>
                  <p>
                    Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis
                    doloribus asperiores repellat.
                  </p>
                </div>
              </div>
              <div class="comment-blog-inner">
                <div class="avatar-blog">
                  <img src="<?//php bloginfo('template_directory');?>/assets/images/team/team2.jpg" alt="">
                </div>
                <div class="comment-blog-text">
                  <span>John Marconi</span>
                  <small>August 7, 2018</small>
                  <p>
                    Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis
                    doloribus asperiores repellat.
                  </p>
                </div>
              </div>
            </div>
            <div class="comments-reply">
              <h3>Leave a reply</h3>
              <form class="form-comment">
                <div class="row form-group">
                  <div class="col-lg-6">
                    <input type="text" name="name-field" class="form-control" placeholder="Your name">
                  </div>
                  <div class="col-lg-6">
                    <input type="email" name="name-field" class="form-control" placeholder="Your E-mail">
                  </div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" rows="5" cols="3" placeholder="Your message"></textarea>
                </div>
                <div class="from-group">
                  <a href="#" class="btn btn-red  "> Comment </a>
                </div>
              </form>
            </div> -->
          </div>
          <?php get_template_part('content', 'sidebar'); ?>
        </div>
      </div>
    </div>
  </div>
<?php
get_footer();
