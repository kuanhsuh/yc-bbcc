<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package studiod
 */

?>

	  </div>
  <!-- Footer-->
  <footer class="footer">
    <div class="footer-warpper">
      <div class="footer-top">
        <div class="container-page">
          <div class="footer-top  clearfix">
            <div class="footer-bottom-content clearfix">
              <div class="row">
                <div class="col-lg-4 col-md-4">
                  <div class="logo-footer">
                    <img src="<?php bloginfo('template_directory');?>/assets/images/logo.svg" class="logo-img" alt="">
                    鎰誠數位
                  </div>
                  <div class="text-footer">
                    <p>
                    我們擁有網頁工程師的背景，對於程式品質具有極高的自我要求。在電子商務及數位行銷上更有著豐富的經驗。
                    </p>
                  </div>
                  <ul class="list-social list-inline">
                    <li>
                      <a href="https://www.facebook.com/bigboycancode/" class="btn btn-circle btn-facebook">
                        <i class="social_facebook "></i>
                      </a>
                    </li>
                    <!-- <li>
                      <a href="#" class="btn btn-circle btn-twitter">
                        <i class="social_twitter "></i>
                      </a>
                    </li>-->
                    <li>
                      <a href="https://www.youtube.com/c/bigboycancode" class="btn btn-circle btn-google-plus">
                        <i class="social_youtube"></i>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/bigboycancode/" class="btn btn-circle btn-instgram">
                        <i class="social_instagram"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-4 col-md-4 mt-3 mt-md-0">
                  <h5>導覽列</h5>
                  <ul class="list-menu">
                    <li>
                      <a href="<?php echo home_url(); ?>">首頁</a>
                    </li>
                    <li>
                      <a href="<?php echo get_page_link(2); ?>">作品</a>
                    </li>
                    <li>
                      <a href="<?php echo get_page_link(47); ?>">部落格</a>
                    </li>
                    <li>
                      <a href="<?php echo get_page_link(44); ?>">聯絡</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-4 col-md-4 mt-3 mt-md-0">
                  <h5>聯絡 </h5>
                  <ul class="list-contact">
                    <li>
                      高雄市三民區義華路384號12F
                    </li>
                    <li>TEL: 0981-606-580</li>
                    <!-- <li>FAX: +7 385-9999</li> -->
                    <li>kuanhsuh@gmail.com</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <!-- COPYRIGHT TEXT -->
        <div class="copyright">
          <p>2018 © Copyright 鎰誠 All rights Reserved.</p>
        </div>
        <!-- COPYRIGHT TEXT -->
      </div>
    </div>
  </footer>

<?php wp_footer(); ?>

</body>
</html>
